import sys
import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains


class TestCasosAprobatorios(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(30)
        self.driver.maximize_window()

    def tearDown(self) -> None:
        self.driver.close()

    def test_reto_final_caso_arbitrario1(self):
        driver = self.driver
        driver.get("https://demoqa.com/automation-practice-form")
        # formulary
        text_first_name_input = driver.find_element_by_xpath("//input[@id='firstName']")
        text_last_name_input = driver.find_element_by_xpath("//input[@id='lastName']")
        text_email_input = driver.find_element_by_xpath("//input[@id='userEmail']")
        radio_button_female = driver.find_element_by_xpath(
            "//body/div[@id='app']/div[1]/div[1]/div[2]/div[2]/div[1]/form[1]/div[3]/div[2]/div[2]"
        )
        text_mobile_input = driver.find_element_by_xpath("//input[@id='userNumber']")
        select_date_input = driver.find_element_by_xpath("//input[@id='dateOfBirthInput']")
        text_subjects_input = driver.find_element_by_xpath("//input[@id='subjectsInput']")
        radio_button_sports_input = driver.find_element_by_xpath(
            "//body/div[@id='app']/div[1]/div[1]/div[2]/div[2]/div[1]/form[1]/div[7]/div[2]/div[1]"
        )
        select_picture_input = driver.find_element_by_xpath("//input[@id='uploadPicture']")
        text_current_textarea = driver.find_element_by_xpath("//textarea[@id='currentAddress']")
        text_state_input = driver.find_element_by_xpath("//input[@id='react-select-3-input']")
        text_city_input = driver.find_element_by_xpath("//input[@id='react-select-4-input']")
        submit_button = driver.find_element_by_xpath("//button[@id='submit']")

        text_first_name_input.send_keys('Maria')
        time.sleep(2)
        text_last_name_input.send_keys('Yepez')
        time.sleep(2)
        text_email_input.send_keys('myepez@canvia.com')
        time.sleep(2)
        radio_button_female.click()
        time.sleep(2)
        text_mobile_input.send_keys('9876543210')
        time.sleep(2)
        select_date_input.click()
        time.sleep(2)
        select_date_input.send_keys(Keys.LEFT_SHIFT + Keys.HOME)
        select_date_input.send_keys(Keys.DELETE)
        select_date_input.send_keys('1990/09/26')
        select_date_input.send_keys(Keys.DELETE)
        select_date_input.send_keys(Keys.ENTER)
        time.sleep(2)
        text_subjects_input.send_keys('E')
        text_subjects_input.send_keys(Keys.ENTER)
        time.sleep(2)
        radio_button_sports_input.click()
        time.sleep(2)
        select_picture_input.send_keys(
            "C:\\Users\\CANVIA\\Documents\\Scanned Documents\\Digitalizacion de bienvenida.jpg")
        time.sleep(2)
        text_current_textarea.send_keys('Prueba Final')
        time.sleep(2)
        text_state_input.send_keys('Haryana')
        text_state_input.send_keys(Keys.ENTER)
        time.sleep(2)
        text_city_input.send_keys('Karnal')
        text_city_input.send_keys(Keys.ENTER)
        submit_button.click()
        time.sleep(2)
        text_output_message = driver.find_element_by_xpath("//div[@id='example-modal-sizes-title-lg']")
        self.assertEqual('Thanks for submitting the form', text_output_message.text)
        time.sleep(5)

    def test_reto_final_caso_arbitrario2(self):
        driver = self.driver
        driver.get("https://demoqa.com/text-box")
        # formulary
        text_username_input = driver.find_element_by_xpath("//input[@id='userName']")
        text_email_input = driver.find_element_by_xpath("//input[@id='userEmail']")
        text_current_address_textarea = driver.find_element_by_xpath("//textarea[@id='currentAddress']")
        text_permanent_address_textarea = driver.find_element_by_xpath("//textarea[@id='permanentAddress']")
        submit_button = driver.find_element_by_xpath("//button[@id='submit']")

        text_username_input.send_keys('Maria Yepez')
        time.sleep(2)
        text_email_input.send_keys('myepez@canvia.com')
        time.sleep(2)
        text_current_address_textarea.send_keys('Prueba Final')
        time.sleep(2)
        text_permanent_address_textarea.send_keys('Python - Selenium')
        time.sleep(2)
        submit_button.click()
        time.sleep(2)
        text_output_name = driver.find_element_by_xpath("//p[@id='name']")
        self.assertEqual('Name:Maria Yepez', text_output_name.text)
        text_output_email = driver.find_element_by_xpath("//p[@id='email']")
        self.assertEqual('Email:myepez@canvia.com', text_output_email.text)
        text_output_current = driver.find_element_by_xpath("//p[@id='currentAddress']")
        self.assertEqual('Current Address :Prueba Final', text_output_current.text)
        text_output_permanent = driver.find_element_by_xpath("//p[@id='permanentAddress']")
        self.assertEqual('Permananet Address :Python - Selenium', text_output_permanent.text)
        time.sleep(5)

    def test_reto_final_caso_arbitrario3(self):
        driver = self.driver
        driver.get("https://demoqa.com/radio-button")

        radio_button_yes_input = driver.find_element_by_xpath("//label[contains(text(),'Yes')]")
        radio_button_yes_input.click()
        time.sleep(5)
        text_output = driver.find_element_by_xpath(
            "//body/div[@id='app']/div[1]/div[1]/div[2]/div[2]/div[1]/p[1]"
        )
        self.assertRegex(text_output.text, "You have selected Yes")

    def test_reto_final_caso_arbitrario4(self):
        driver = self.driver
        driver.get("https://demoqa.com/radio-button")

        radio_button_impressive_input = driver.find_element_by_xpath("//label[contains(text(),'Impressive')]")
        radio_button_impressive_input.click()
        time.sleep(5)
        text_output = driver.find_element_by_xpath(
            "//body/div[@id='app']/div[1]/div[1]/div[2]/div[2]/div[1]/p[1]"
        )
        self.assertRegex(text_output.text, "You have selected Impressive")

    def test_reto_final_caso_arbitrario5(self):
        driver = self.driver
        driver.get("https://demoqa.com/buttons")

        button_double_click = driver.find_element_by_xpath("//button[@id='doubleClickBtn']")
        actionchains = ActionChains(driver)
        actionchains.double_click(button_double_click).perform()
        button_double_click.click()
        time.sleep(5)
        text_output_double = driver.find_element_by_xpath("//p[@id='doubleClickMessage']")
        self.assertRegex(text_output_double.text, "You have done a double click")


if __name__ == '__main__':
    unittest.main()
