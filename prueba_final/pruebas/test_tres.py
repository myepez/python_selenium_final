import unittest
import time
from selenium import webdriver


class TestTres(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(30)
        self.driver.maximize_window()

    def tearDown(self) -> None:
        self.driver.close()

    def test_tres_yes(self):
        driver = self.driver
        driver.get("https://demoqa.com/radio-button")

        radio_button_yes_input = driver.find_element_by_xpath("//label[contains(text(),'Yes')]")
        radio_button_yes_input.click()
        time.sleep(5)
        text_output = driver.find_element_by_xpath(
            "//body/div[@id='app']/div[1]/div[1]/div[2]/div[2]/div[1]/p[1]"
        )
        self.assertRegex(text_output.text, "You have selected Yes")

    def test_tres_impressive(self):
        driver = self.driver
        driver.get("https://demoqa.com/radio-button")

        radio_button_impressive_input = driver.find_element_by_xpath("//label[contains(text(),'Impressive')]")
        radio_button_impressive_input.click()
        time.sleep(5)
        text_output = driver.find_element_by_xpath(
            "//body/div[@id='app']/div[1]/div[1]/div[2]/div[2]/div[1]/p[1]"
        )
        self.assertRegex(text_output.text, "You have selected Impressive")

if __name__ == '__main__':
    unittest.main()
