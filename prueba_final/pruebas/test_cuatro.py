import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By


class TestCuatro(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(30)
        self.driver.maximize_window()

    def tearDown(self) -> None:
        self.driver.close()

    @unittest.skip("temp")
    def test_cuatro_double_click(self):
        driver = self.driver
        driver.get("https://demoqa.com/buttons")

        button_double_click = driver.find_element_by_xpath("//button[@id='doubleClickBtn']")
        actionchains = ActionChains(driver)
        actionchains.double_click(button_double_click).perform()
        button_double_click.click()
        time.sleep(5)
        text_output_double = driver.find_element_by_xpath("//p[@id='doubleClickMessage']")
        self.assertRegex(text_output_double.text, "You have done a double click")

    @unittest.skip("temp")
    def test_cuatro_right_click(self):
        driver = self.driver
        driver.get("https://demoqa.com/buttons")

        button_right_click = driver.find_element_by_xpath("//button[@id='rightClickBtn']")
        actionchains = ActionChains(driver)
        actionchains.context_click(button_right_click).perform()
        button_right_click.click()
        time.sleep(5)
        text_output_right = driver.find_element_by_xpath("//p[@id='rightClickMessage']")
        self.assertRegex(text_output_right.text, "You have done a right click")

if __name__ == '__main__':
    unittest.main()
