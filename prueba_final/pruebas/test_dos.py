import unittest
import time
from selenium import webdriver


class TestDos(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(30)
        self.driver.maximize_window()

    def tearDown(self) -> None:
        self.driver.close()

    def test_dos(self):
        driver = self.driver
        driver.get("https://demoqa.com/text-box")
        # formulary
        text_username_input = driver.find_element_by_xpath("//input[@id='userName']")
        text_email_input = driver.find_element_by_xpath("//input[@id='userEmail']")
        text_current_address_textarea = driver.find_element_by_xpath("//textarea[@id='currentAddress']")
        text_permanent_address_textarea = driver.find_element_by_xpath("//textarea[@id='permanentAddress']")
        submit_button = driver.find_element_by_xpath("//button[@id='submit']")

        text_username_input.send_keys('Maria Yepez')
        time.sleep(2)
        text_email_input.send_keys('myepez@canvia.com')
        time.sleep(2)
        text_current_address_textarea.send_keys('Prueba Final')
        time.sleep(2)
        text_permanent_address_textarea.send_keys('Python - Selenium')
        time.sleep(2)
        submit_button.click()
        time.sleep(2)
        text_output_name = driver.find_element_by_xpath("//p[@id='name']")
        self.assertEqual('Name:Maria Yepez', text_output_name.text)
        text_output_email = driver.find_element_by_xpath("//p[@id='email']")
        self.assertEqual('Email:myepez@canvia.com', text_output_email.text)
        text_output_current = driver.find_element_by_xpath("//p[@id='currentAddress']")
        self.assertEqual('Current Address :Prueba Final', text_output_current.text)
        text_output_permanent = driver.find_element_by_xpath("//p[@id='permanentAddress']")
        self.assertEqual('Permananet Address :Python - Selenium', text_output_permanent.text)
        time.sleep(5)

if __name__ == '__main__':
    unittest.main()
